## Gitlab pages

create new repo and add `.gitlab-ci.yml` file:

	image: alpine:latest
	pages:
	  stage: deploy
	  script:
	  - echo 'Nothing to do...'
	  artifacts:
	    paths:
	    - public
	  only:
	  - master

for testing create file `public/index.html`

visit: https://suhailvs.gitlab.io/angular_curd/

## Angular Static site
take production build from https://github.com/suhailvs/angular_curd/frontend/

	npm install
	ng build --prod --base-href "https://suhail.pw/"

Move to gitlab folder

	mv dist/frontend ~/gitlab/angular_curd/public


## Custom Domain

### Add a custom domain to Pages

Navigate to your project’s `Setting > Pages` and click `+ New domain` then enter domain name then click `Create New Domain`

### Set up DNS records at Godaddy:

1. Add **A** record: Host: `@`, Points to: `35.185.44.232`

2. Add **TXT** record: Host: `@`, Value: `_gitlab-pages-verification-code.suhail.pw TXT gitlab-pages-verification-code=be5b57b2edf108411c9533f6a0accd15`




For more documentation vist <https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/>